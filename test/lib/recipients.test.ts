import { expect } from "@oclif/test";
import { loadRecipientsFromCSV } from "../../src/lib/recipients";

describe("loadRecipientsFromCSV", () => {
  it("should load a CSV string into an array of objects", () => {
    const csvString = [
      "email,name",
      "user@example.com,User Name",
      "test@example.com,Different User",
    ].join("\n");

    const csvObject = [
      {
        email: "user@example.com",
        name: "User Name",
      },
      {
        email: "test@example.com",
        name: "Different User",
      },
    ];
    expect(loadRecipientsFromCSV(csvString)).to.deep.equal(csvObject);
  });

  it("should produce a left if errors in csvstring", () => {
    const csvString = [
      "email,name",
      "user@example.comUser Name",
      "test@example.com,Different User,something",
    ].join("\n");
    expect(() => loadRecipientsFromCSV(csvString)).to.throw();
  });
});
