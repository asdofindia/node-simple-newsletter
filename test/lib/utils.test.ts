import { expect } from "@oclif/test";
import * as path from "path";

import { readFile } from "../../src/lib/utils";

describe("readFile", () => {
  it("should read some file", () => {
    const stringInFile = "SOMESTRING";
    const filePath = path.join(__dirname, "../sample/readFile");
    expect(readFile(filePath)).to.deep.equal(stringInFile);
  });
});
