node-simple-newsletter
======================

A simple command line tool to send newsletters using an SMTP server

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/node-simple-newsletter.svg)](https://npmjs.org/package/node-simple-newsletter)
[![Downloads/week](https://img.shields.io/npm/dw/node-simple-newsletter.svg)](https://npmjs.org/package/node-simple-newsletter)
[![License](https://img.shields.io/npm/l/node-simple-newsletter.svg)](https://gitlab.com/asdofindia/node-simple-newsletter/-/blob/main/package.json)

Install globally, setup `.env`, `recipients.csv`, and `template.pug` in the working directory. See configuration below.

<!-- toc -->
* [Configuration](#configuration)
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Configuration

```
$ cp sample.env .env
$ cat .env
SMTP_HOST=mail.example.com
SMTP_PORT=587
SMTP_USE_TLS=FALSE
SMTP_USER=test@example.com
SMTP_PASSWORD=password

FROM=Random User <test@example.com>
LIST_ID=<http://newsletter.example.com>
UNSUBSCRIBE=mailto:test@example.com?subject=unsubscribe-newsletter

$ $EDITOR .env  # change the parameters that works with your SMTP server

$ cat recipients.csv
name,email
Goodie Reader,reader@example.com
Yankie Bot,bot@example.com

$ cat template.pug
p Hello #{name}
p This is a newsletter
p Bye bye
```

# Usage
<!-- usage -->
```sh-session
$ npm install -g node-simple-newsletter
$ node-simple-newsletter COMMAND
running command...
$ node-simple-newsletter (-v|--version|version)
node-simple-newsletter/0.0.6 linux-x64 node-v16.0.0
$ node-simple-newsletter --help [COMMAND]
USAGE
  $ node-simple-newsletter COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`node-simple-newsletter help [COMMAND]`](#node-simple-newsletter-help-command)
* [`node-simple-newsletter send`](#node-simple-newsletter-send)
* [`node-simple-newsletter view [TEMPLATE]`](#node-simple-newsletter-view-template)

## `node-simple-newsletter help [COMMAND]`

display help for node-simple-newsletter

```
USAGE
  $ node-simple-newsletter help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.2.2/src/commands/help.ts)_

## `node-simple-newsletter send`

Send the newsletter

```
USAGE
  $ node-simple-newsletter send

OPTIONS
  -h, --help                   show CLI help
  -r, --recipients=recipients  [default: recipients.csv] Recipients CSV
  -s, --subject=subject        [default: Untitled Newsletter Edition] Subject
  -t, --template=template      [default: template.pug] Template

  --toEmail=toEmail            [default: email] The field in the csv that represents the recipient email address (used
                               in calculating the "To" field, and address)

  --toName=toName              [default: name] The field in the csv that represents the recipient name (used in
                               calculating the "To" field)

EXAMPLE
  $ node-simple-newsletter send -s "How is it going?"
  Sending 22 mails
```

_See code: [src/commands/send.ts](https://gitlab.com/asdofindia/node-simple-newsletter/blob/v0.0.6/src/commands/send.ts)_

## `node-simple-newsletter view [TEMPLATE]`

View compiled template

```
USAGE
  $ node-simple-newsletter view [TEMPLATE]

ARGUMENTS
  TEMPLATE  [default: template.pug] Path to template to preview

OPTIONS
  -h, --help  show CLI help

EXAMPLE
  $ node-simple-newsletter view template.pug
  Opening browser
```

_See code: [src/commands/view.ts](https://gitlab.com/asdofindia/node-simple-newsletter/blob/v0.0.6/src/commands/view.ts)_
<!-- commandsstop -->
