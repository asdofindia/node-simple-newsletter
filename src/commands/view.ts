import { Command, flags } from "@oclif/command";
import * as debug from "debug";
import { getPuggit } from "../lib/template";
import { fileInCwd, writeToTempFile } from "../lib/utils";
import * as open from "open";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = debug("newsletter-send");

export default class View extends Command {
  static description = "View compiled template";

  static examples = [
    `$ node-simple-newsletter view template.pug
Opening browser
`,
  ];
  static args = [
    {
      name: "TEMPLATE",
      default: "template.pug",
      description: "Path to template to preview",
    },
  ];
  static flags = {
    help: flags.help({ char: "h" }),
  };

  async run(): Promise<void> {
    const { args } = this.parse(View);
    const templatePath = fileInCwd(args.TEMPLATE);
    const puggit = getPuggit(templatePath);
    const html = puggit();
    const tempfile = writeToTempFile(html, "template.html");
    open(`${tempfile}`);
    this.log(`Opening file://${tempfile} in default browser`);
  }
}
