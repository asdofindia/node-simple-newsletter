import { Command, flags } from "@oclif/command";
import * as debug from "debug";
import { SentMessageInfo } from "nodemailer";
import { composeMail } from "../lib/mail";
import { logSentMessageInfo, transporter } from "../lib/mailer";
import { getRecipients, getSMTPToFieldGenerator } from "../lib/recipients";
import { fileInCwd } from "../lib/utils";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const log = debug("newsletter-send");

export default class Send extends Command {
  static description = "Send the newsletter";

  static examples = [
    `$ node-simple-newsletter send -s "How is it going?"
Sending 22 mails
`,
  ];

  static flags = {
    help: flags.help({ char: "h" }),
    subject: flags.string({
      char: "s",
      description: "Subject",
      default: "Untitled Newsletter Edition",
    }),
    recipients: flags.string({
      char: "r",
      description: "Recipients CSV",
      default: "recipients.csv",
    }),
    template: flags.string({
      char: "t",
      description: "Template",
      default: "template.pug",
    }),
    toEmail: flags.string({
      description:
        'The field in the csv that represents the recipient email address (used in calculating the "To" field, and address)',
      default: "email",
    }),
    toName: flags.string({
      description:
        'The field in the csv that represents the recipient name (used in calculating the "To" field)',
      default: "name",
    }),
  };

  async run(): Promise<void> {
    const { flags } = this.parse(Send);
    const subject = flags.subject;
    const recipientsPath = fileInCwd(flags.recipients);
    const templatePath = fileInCwd(flags.template);

    const toFunc = getSMTPToFieldGenerator(flags.toEmail, flags.toName);

    const recipients = getRecipients(recipientsPath);

    this.log(`Sending ${recipients.length} mails`);

    let mailsSent = 0;
    const mailDeliveryCallback = (
      err: Error | null,
      info: SentMessageInfo
    ): void => {
      mailsSent += 1;
      if (err) {
        this.log(`Error in sending mail ${mailsSent}`);
      } else {
        this.log(`Mail ${mailsSent}: ${logSentMessageInfo(info)}`);
      }
      if (mailsSent === recipients.length) {
        transporter.close();
      }
    };
    for (const recipient of recipients) {
      const to = toFunc(recipient);
      const mail = composeMail({ recipient, to, subject, templatePath });
      transporter.sendMail(mail, mailDeliveryCallback);
    }
  }
}
