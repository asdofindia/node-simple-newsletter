import * as dotenv from "dotenv";

dotenv.config();

export interface MailerConfiguration {
  smtpHost: string;
  smtpPort: string;
  smtpUseTls: string;
  smtpUser: string;
  smtpPass: string;
}

export interface ListConfiguration {
  from: string;
  listId: string;
  unsubscribe: string;
}

type Configuration = MailerConfiguration & ListConfiguration;

export const configuration: Configuration = {
  smtpHost: process.env.SMTP_HOST || "smtp.gmail.com",
  smtpPort: process.env.SMTP_PORT || "465",
  smtpUseTls: process.env.SMTP_USE_TLS || "FALSE",
  smtpUser: process.env.SMTP_USER || "username",
  smtpPass: process.env.SMTP_PASSWORD || "password",

  from: process.env.FROM || "test@example.com",
  listId: process.env.LIST_ID || "<newsletter.example.com>",
  unsubscribe:
    process.env.UNSUBSCRIBE || "newsletter@example.com?subject=unsubscribe",
};
