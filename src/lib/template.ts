import { readFile } from "./utils";
import { compileFile, compileTemplate, LocalsObject } from "pug";

export const getMailTemplate = readFile;

export const sanitizeMessageTemplate = (messageTemplate: string): string =>
  messageTemplate.replace(/`/g, "\\`");

export const getSanitizedMessageTemplate = (filename: string): string =>
  sanitizeMessageTemplate(getMailTemplate(filename));

export const getPuggit = compileFile;

export const getHtml = (
  puggit: compileTemplate,
  options: LocalsObject
): string => puggit(options);
