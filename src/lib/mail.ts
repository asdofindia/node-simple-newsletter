import { getPuggit } from "./template";
import { htmlToText } from "html-to-text";
import { Recipient } from "./recipients";
import Mail = require("nodemailer/lib/mailer");
import { configuration } from "./config";

const fromAddress = configuration.from;

const headers = {
  "List-Id": configuration.listId,
};

const listDetails = {
  unsubscribe: configuration.unsubscribe,
};

interface ComposeOptions {
  recipient: Recipient;
  subject: string;
  to: string;
  templatePath: string;
}

export const composeMail = ({
  recipient,
  subject,
  templatePath,
  to,
}: ComposeOptions): Mail.Options => {
  const puggit = getPuggit(templatePath);
  const html = puggit(recipient);
  const text = htmlToText(html);
  const from = fromAddress;
  const list = listDetails;
  return {
    from,
    to,
    subject,
    html,
    text,
    list,
    headers,
  };
};
