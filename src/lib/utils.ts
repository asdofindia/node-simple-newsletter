import { readFileSync, writeFileSync } from "fs";
import { tmpdir } from "os";
import { join } from "path";

export const readFile = (filename: string): string =>
  readFileSync(filename, "utf-8");

export const fileInCwd = (filePath: string): string =>
  join(process.cwd(), filePath);

export const writeFile = (filename: string, data: string): void =>
  writeFileSync(filename, data);

const tempfile = (name?: string): string =>
  join(tmpdir(), `node-simple-newsletter-${name || new Date().getTime()}`);

export const writeToTempFile = (data: string, name?: string): string => {
  const tempfilename = tempfile(name);
  writeFile(tempfilename, data);
  return tempfilename;
};
