import debug from "debug";
import { createTransport, SentMessageInfo } from "nodemailer";
import { configuration } from "./config";

const log = debug("newsletter-send-internals");

const host = configuration.smtpHost;
const port = parseInt(configuration.smtpPort, 10);
const pool = true;

// nodemailer sets tls to false by default. So, we do the same too.
const secure = ["TRUE", "T", "1"].includes(
  configuration.smtpUseTls.toUpperCase() || "FALSE"
);
const auth = {
  user: configuration.smtpUser,
  pass: configuration.smtpPass,
};

log(`Creating transporter with following options:`);
log(
  `   smtp${secure ? "s" : ""}://${auth.user}@${
    auth.pass
  }:${host}:${port}/?pool=${pool}`
);

export const transporter = createTransport({
  pool,
  host,
  port,
  secure,
  auth,
});

export const logSentMessageInfo = (info: SentMessageInfo): string => {
  let output = `${info.messageId}: `;
  if (info.accepted?.length) {
    output += `${info.accepted} (accepted) |`;
  }
  if (info.rejected?.length) {
    output += `${info.rejected} (rejected) |`;
  }
  if (info.pending?.length) {
    output += `${info.pending} (pending) |`;
  }
  return output;
};
