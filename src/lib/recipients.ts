import { parse } from "papaparse";
import { readFile } from "./utils";

export type Recipient = {
  [key: string]: string;
};

export const loadRecipientsFromCSV = (csvString: string): Recipient[] => {
  const result = parse<Recipient>(csvString, {
    header: true,
    delimiter: ",",
    skipEmptyLines: true,
  });
  if (result.errors.length === 0) return result.data;
  throw result.errors;
};

export const getRecipients = (filename: string): Recipient[] =>
  loadRecipientsFromCSV(readFile(filename));

export const getSMTPToFieldGenerator = (
  emailField: string,
  nameField: string
) => (recipient: Recipient): string =>
  nameField in recipient
    ? `"${recipient[nameField]}" <${recipient[emailField]}>`
    : recipient[emailField];
